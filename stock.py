# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from sql.aggregate import Max
from sql.conditionals import Coalesce
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction

__all__ = ['Move']


class Move:
    __name__ = 'stock.move'
    __metaclass__ = PoolMeta

    last_prices = fields.Function(
        fields.Many2Many('account.invoice.line', None, None, 'Last prices',
            domain=[('product', '=', Eval('product'))],
            depends=['product']),
        'get_last_prices')

    @fields.depends('product', 'shipment', 'effective_date', 'planned_date')
    def on_change_with_last_prices(self):
        return self.get_last_prices([self])[self.id]

    @classmethod
    def get_last_prices(cls, records, name=None):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        InvoiceLineMove = pool.get('account.invoice.line-stock.move')
        Invoiceline = pool.get('account.invoice.line')
        invoice = Invoice.__table__()
        line = Invoiceline.__table__()
        invoice_move = InvoiceLineMove.__table__()
        move = cls.__table__()
        cursor = Transaction().connection.cursor()

        res = {r.id: [] for r in records}
        for record in records:
            if not record.shipment:
                continue
            if (not record.shipment.__name__.endswith('.in') and
                    not record.shipment.__name__.endswith('.out')):
                continue
            if not record.product:
                continue
            _, _, _type = record.shipment.__name__.split('.')
            if _type == 'in':
                party_id = record.shipment.supplier.id
            else:
                party_id = record.shipment.customer.id
            max_date = (record.effective_date or record.planned_date or
                record.shipment.effective_date or record.shipment.planned_date)
            if not max_date:
                continue
            cursor.execute(*line.join(
                invoice, condition=(invoice.id == line.invoice)
                ).join(invoice_move, condition=(
                    invoice_move.invoice_line == line.id)
                ).join(move, condition=(move.id == invoice_move.stock_move)
                ).select(Max(line.id),
                       Max(Coalesce(move.effective_date, move.planned_date)),
                       where=(
                        (line.type == 'line') &
                        (move.shipment != '%s,%s' % (
                            record.shipment.__name__, record.shipment.id)) &
                        (line.product == record.product.id) &
                        (invoice.party == party_id) &
                        (invoice.state.in_(['posted', 'paid'])) &
                        (invoice.type == _type) &
                        (Coalesce(Coalesce(move.effective_date,
                            move.planned_date), invoice.invoice_date
                        ) <= max_date)),
                       order_by=(line.product, invoice.invoice_date.desc),
                       group_by=(line.product, invoice.invoice_date,
                            line.invoice),
                       limit=3)
                )
            res[record.id] = [row[0] for row in cursor.fetchall()] or []
        return res
