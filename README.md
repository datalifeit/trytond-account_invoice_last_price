datalife_account_invoice_last_price
===================================

The account_invoice_last_price module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-account_invoice_last_price/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-account_invoice_last_price)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
